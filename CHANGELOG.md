# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.5](https://gitlab.com/chet.manley/node-project-templates/compare/v1.0.4...v1.0.5) (2020-12-12)

### [1.0.4](https://gitlab.com/chet.manley/node-project-templates/compare/v1.0.3...v1.0.4) (2020-11-25)

### [1.0.3](https://gitlab.com/chet.manley/node-project-templates/compare/v1.0.2...v1.0.3) (2020-09-16)

### [1.0.2](https://gitlab.com/chet.manley/node-project-templates/compare/v1.0.1...v1.0.2) (2020-09-16)

### [1.0.1](https://gitlab.com/chet.manley/node-project-templates/compare/v1.0.0...v1.0.1) (2020-09-16)


### Bug Fixes

* **bug:** npm install does not install certain files ([d4775b1](https://gitlab.com/chet.manley/node-project-templates/commit/d4775b13dbcf3b47cec8d3cb003dfad7b2d016a1))

## [1.0.0](https://gitlab.com/chet.manley/node-project-templates/compare/v0.0.3...v1.0.0) (2020-09-15)

### [0.0.3](https://gitlab.com/chet.manley/node-project-templates/compare/v0.0.2...v0.0.3) (2020-09-15)


### Bug Fixes

* **bug:** only lint files in source to prevent ESlint failure during lintstaged commit ([417af12](https://gitlab.com/chet.manley/node-project-templates/commit/417af1265ad29809e5b13ee57db77565a1fd0782))

### [0.0.2](https://gitlab.com/chet.manley/node-project-templates/compare/v0.0.1...v0.0.2) (2020-09-14)


### Bug Fixes

* **bug:** do not exclude test files from published templates ([e55601f](https://gitlab.com/chet.manley/node-project-templates/commit/e55601f8ca0a701fbf1bd8d66286bff939575474))
* **ci:** invalid syntax in if expression ([77c1205](https://gitlab.com/chet.manley/node-project-templates/commit/77c1205f9ede28b05585ae0516bb69ade0606467))
* **ci:** remove bad image tags ([ea8ea4f](https://gitlab.com/chet.manley/node-project-templates/commit/ea8ea4fd2850bdb2048dd907dc68485a8c5b887c))
* **ci:** remove invalid cache syntax ([fdbdbaf](https://gitlab.com/chet.manley/node-project-templates/commit/fdbdbafcb435fece87b2a877f0bf8439c488272d))
* **ci:** trigger pipelines on master branch ([e3bd60a](https://gitlab.com/chet.manley/node-project-templates/commit/e3bd60aba956f30a18d0038061d9700749ccf516))
* **ci:** use correct image tag variable ([69f3687](https://gitlab.com/chet.manley/node-project-templates/commit/69f368717112349020f6c0ac4ab226864fefe068))

### 0.0.1 (2020-09-01)
