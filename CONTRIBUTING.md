# Contributing

> :warning:\
> _Before attempting to create a new contribution to this project,
> please search existing issues first to help prevent duplicates.
> If none exist, [create a new issue](https://gitlab.com/chet.manley/node-project-templates/-/issues/new)
> so that the change(s) can be discussed, vetted, and tracked._

<!-- MD028 -->

> :information_source:\
> First-time open-source contributors,
> or anyone looking to familiarize themselves with this project's codebase,
> can sort issues by the ~"good first issue" and ~"accepting merge requests" labels.

## Getting Started

- [Create a fork](https://gitlab.com/chet.manley/node-project-templates/-/forks/new) of this project.
- Clone the new fork to your local environment:
  - `git clone https://gitlab.com/<your namespace>/node-project-templates.git`
- Install dependencies:
  - `npm install`
- Checkout the `integration` branch.
  Merge requests to `master` or `release*` branches are automatically denied.
  - `git checkout integration`
- Create a new branch off of the `integration` branch.
  Branch names must be either `feature/*` for features or `bugfix/*` for bug fixes.
  - `git checkout -b feature/your-cool-feature`
- Create your changes.
- Create tests that cover your changes.
- Run test suite:
  - `npm run test:all`
- Once all tests pass, push your changes:
  - `git push`
- Create a merge request
(`https://gitlab.com/<your namespace>/node-project-templates/-/merge_requests/new?merge_request%5Btarget_branch%5D=integration`).
  - Select your new branch as the source branch.
  - Select `chet.manley/node-project-templates` as the target repository.
  - Select `integration` as the target branch.
- Choose an appropriate merge request template from the dropdown.
- Each merge request is expected to resolve an issue,
  and each template has a placeholder for this info.
  Ensure that you include your issue reference number (created prior to forking)
  in the appropriate section.
- Once submitted, your code will be tested in this project's CI pipeline,
  and any further discussion of changes will take place in the merge request.

> :information_source:\
> **_This project enforces strict code style and commit messages via git hooks.
> Failing either will cancel the commit._**
>
> - Code must comply with the [standardJS style guide](https://standardjs.com/rules.html).
> - Commit messages must comply with the [Conventional Commits standard](https://www.conventionalcommits.org/) in order for the versioning system to work properly.
>
> A sample commit message with basic options is included in the repo,
> and I recommend using it as a commit message template for this project:
>
> ```shell
> cd /path/to/project/
> git config --add commit.template .git-commit-message
> ```

## Testing

[![Jest](https://img.shields.io/static/v1?logo=jest&label=Jest&message=✔&color=C21325&style=flat)][jest-url]
[![ESLint](https://img.shields.io/static/v1?logo=eslint&label=ESLint&message=✔&color=4B32C3&style=flat)][eslint-url]
[![standardJS](https://img.shields.io/static/v1?logo=eslint&label=standardJS&message=✔&color=f3df49&style=flat)][standardjs-url]

This project employs the following local checks to aid in development:

- [Jest][jest-url] for unit and coverage tests.
- [ESlint][eslint-url] to enforce recommended use and formatting in Jest tests.
- [standardJS][standardjs-url] to enforce a strict style guide.

If you are unfamiliar with Jest, I recommend reading their
[Getting Started doc](https://jestjs.io/docs/en/getting-started),
as well as reviewing the tests currently included in this project.
<!-- Merge requests without requisite unit tests will not be approved. -->

[eslint-url]: https://eslint.org/
[jest-url]: https://jestjs.io/
[standardjs-url]: https://standardjs.com/

> :information_source:\
> Run unit tests and display a short text coverage report:
>
> ```shell
> npm test
> ```

<!-- MD028 -->

> :information_source:\
> Continuously run unit tests on file changes:
>
> ```shell
> npm run test:watch
> ```

<!-- MD028 -->

> :information_source:\
> Verify that code complies with the [standardJS style guide](https://standardjs.com/rules.html):
>
> ```shell
> npm run test:lint
> ```
