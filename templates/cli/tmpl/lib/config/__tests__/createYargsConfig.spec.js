'use strict'

jest.unmock('../createYargsConfig')
const createYargsConfig = require('../createYargsConfig')

const defaults = require('../defaults.json')

describe('createYargsConfig module', () => {
  it('exports createYargsConfig function', () => {
    expect(typeof createYargsConfig).toEqual('function')
    expect(createYargsConfig.name).toEqual('createYargsConfig')
  })

  describe('createYargsConfig function', () => {
    it('throws if no arguments passed', () => {
      expect(createYargsConfig).toThrow()
    })

    it('returns `options` key', () => {
      expect(createYargsConfig({})).toHaveProperty('options')
    })

    it('returns `positionals` key', () => {
      expect(createYargsConfig({})).toHaveProperty('positionals')
    })

    it('returns `usage` key', () => {
      expect(createYargsConfig({})).toHaveProperty('usage')
    })

    it('adds defaults to config', () => {
      expect(createYargsConfig(defaults)).toHaveProperty(
        'options.D.default',
        defaults.debug
      )
    })
  })
})
