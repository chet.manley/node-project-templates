'use strict'

jest.unmock('yargs')
jest.unmock('../createOptionsFromArgs')
const createOptionsFromArgs = require('../createOptionsFromArgs')

const defaults = require('../defaults.json')

describe('createOptionsFromArgs module', () => {
  it('exports createOptionsFromArgs function', () => {
    expect(typeof createOptionsFromArgs).toEqual('function')
    expect(createOptionsFromArgs.name).toEqual('createOptionsFromArgs')
  })

  describe('createOptionsFromArgs function', () => {
    const consoleErrorSpy = jest.spyOn(console, 'error')
      .mockImplementation(_ => null)
    const processExitSpy = jest.spyOn(process, 'exit')
      .mockImplementation(_ => { throw new Error('EXITS') })

    beforeEach(() => {
      consoleErrorSpy.mockClear()
      processExitSpy.mockClear()
    })

    it('exits if no arguments passed', () => {
      expect(createOptionsFromArgs).toThrow('EXITS')
      expect(processExitSpy).toHaveBeenCalledTimes(1)
      expect(processExitSpy).toHaveBeenCalledWith(1)
    })

    it('errors if required positionals missing', () => {
      expect(_ => createOptionsFromArgs(['-D'])).toThrow('EXITS')
      expect(consoleErrorSpy).toHaveBeenCalledTimes(3)
      expect(consoleErrorSpy).toHaveBeenLastCalledWith(
        expect.stringContaining('Not enough non-option arguments:')
      )
      expect(processExitSpy).toHaveBeenCalledTimes(1)
      expect(processExitSpy).toHaveBeenCalledWith(1)
    })

    it('errors on unrecognized options', () => {
      expect(_ => createOptionsFromArgs(['pos1', '-FAIL'])).toThrow('EXITS')
      expect(consoleErrorSpy).toHaveBeenCalledTimes(3)
      expect(consoleErrorSpy).toHaveBeenLastCalledWith(
        expect.stringContaining('Unknown arguments: F, A, I, L')
      )
      expect(processExitSpy).toHaveBeenCalledTimes(1)
      expect(processExitSpy).toHaveBeenCalledWith(1)
    })

    it('returns object containing options', () => {
      const options = createOptionsFromArgs(['pos1'])

      expect(processExitSpy).not.toHaveBeenCalled()
      expect(options).toHaveProperty('debug', undefined)
      expect(options).toHaveProperty('verbose', 0)
      expect(options).toHaveProperty('positional1', 'pos1')
    })

    it('accepts optional default config object', () => {
      const createYargsConfig = require('../createYargsConfig')
      createYargsConfig.mockClear()

      expect(createOptionsFromArgs([''], defaults))
        .toHaveProperty('debug', false)
      expect(createYargsConfig).toHaveBeenCalledTimes(1)
      expect(createYargsConfig).toHaveBeenCalledWith(defaults)
    })
  })
})
