'use strict'

jest.unmock('../mergeOptions')
const mergeOptions = require('../mergeOptions')

describe('mergeOptions module', () => {
  it('exports mergeOptions function', () => {
    expect(typeof mergeOptions).toEqual('function')
    expect(mergeOptions.name).toEqual('mergeOptions')
  })

  describe('mergeOptions function', () => {
    it('throws if no arguments passed', () => {
      expect(mergeOptions).toThrow()
    })

    it("only overwrites keys whose value is undefined or ''", () => {
      const options = {
        key1: 'value1',
        key2: false,
        key3: null,
        key4: ''
      }
      const override = {
        key1: 'value2',
        key2: 'value2',
        key3: 'value2',
        key4: 'value2',
        key5: 'value2'
      }
      const result = {
        key1: 'value1',
        key2: false,
        key3: null,
        key4: 'value2',
        key5: 'value2'
      }

      expect(mergeOptions(options, override)).toEqual(result)
      expect(options).toEqual(result)
    })
  })
})
