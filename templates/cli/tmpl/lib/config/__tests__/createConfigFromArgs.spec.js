'use strict'

jest.unmock('../createConfigFromArgs')
const createConfigFromArgs = require('../createConfigFromArgs')

describe('createConfigFromArgs module', () => {
  it('exports createConfigFromArgs function', () => {
    expect(typeof createConfigFromArgs).toEqual('function')
    expect(createConfigFromArgs.name).toEqual('createConfigFromArgs')
  })

  describe('createConfigFromArgs function', () => {
    it('returns config object', () => {
      const config = createConfigFromArgs({ verbose: 2 })

      expect(config).toHaveProperty('debug', false)
      expect(config).toHaveProperty('verbose', 2)
      expect(process.env.DEBUG).toBeFalsy()
    })

    it('sets process-wide debug', () => {
      const config = createConfigFromArgs({ debug: true })

      expect(config).toHaveProperty('debug', true)
      expect(config).toHaveProperty('verbose', 10)
      expect(process.env.DEBUG).toBeTruthy()
    })
  })
})
