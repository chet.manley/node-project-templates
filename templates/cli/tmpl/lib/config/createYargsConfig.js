'use strict'

function createYargsConfig (defaults) {
  return {
    options: {
      /**
       * -d causes npm init to display debug output
       */
      D: {
        alias: 'debug',
        default: defaults.debug,
        hidden: true,
        type: 'boolean'
      },
      /**
       * -v causes npm init to display npm version and exit.
       */
      V: {
        alias: 'verbose',
        description: 'Display verbose output',
        type: 'count'
      }
    },

    positionals: {
      positional1: {
        defaultDescription: 'pos1',
        description: 'this is positional 1',
        nargs: 1,
        requiresArg: true,
        type: 'string'
      },
      positional2: {
        defaultDescription: 'pos2',
        description: 'this is positional 2',
        nargs: 1,
        requiresArg: true,
        type: 'string'
      }
    },

    usage: {
      command: '$0 positional1 [positional2] [options]',
      description: 'NodeJS CLI template'
    }
  }
}

module.exports = exports = createYargsConfig
