'use strict'

const mergeOptions = require('./mergeOptions')
const createOptionsFromArgs = require('./createOptionsFromArgs')

const defaults = require('./defaults')

function createConfigFromArgs (cliArgs) {
  const options = createOptionsFromArgs(cliArgs, defaults)
  options.debug && (process.env.DEBUG = true)

  const config = {
    debug: options.debug,
    verbose: options.debug ? 10 : options.verbose
  }

  mergeOptions(config, defaults)

  return config
}

module.exports = exports = createConfigFromArgs
