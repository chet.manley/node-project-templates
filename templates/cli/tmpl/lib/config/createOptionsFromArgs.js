'use strict'

const yargs = require('yargs')

const createYargsConfig = require('./createYargsConfig')

function createOptionsFromArgs (args, defaults = {}) {
  const config = createYargsConfig(defaults)

  return yargs(args)
    .usage(
      config.usage.command,
      config.usage.description,
      yargs => yargs
        .positional('positional1', config.positionals.positional1)
        .positional('positional2', config.positionals.positional2)
    )
    .options(config.options)
    .strict()
    .argv
}

module.exports = exports = createOptionsFromArgs
