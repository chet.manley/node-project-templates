'use strict'

function mergeOptions (options, defaults) {
  const predicate = val => val !== '' && val !== undefined

  for (const [key, value] of Object.entries(defaults)) {
    predicate(options[key]) || (options[key] = value)
  }

  return options
}

module.exports = exports = mergeOptions
