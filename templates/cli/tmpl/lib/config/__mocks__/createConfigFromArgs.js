'use strict'

module.exports = exports = jest.fn(cliArgs => {
  if (cliArgs.includes('-D')) { process.env.DEBUG = true }
  if (cliArgs.includes('-f')) { throw new Error('Unknown argument: -f') }
  return {}
})
