'use strict'

const index = require('../index')

describe('Package entrypoint', () => {
  it('re-exports cli function', () => {
    expect(typeof index).toEqual('function')
    expect(index.name).toEqual('cli')
  })
})
