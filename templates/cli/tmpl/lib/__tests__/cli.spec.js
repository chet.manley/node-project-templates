'use strict'

jest.unmock('../cli')
const cli = require('../cli')

const consoleDebugSpy = jest.spyOn(console, 'debug').mockImplementation(_ => {})
const consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation(_ => {})
jest.spyOn(console, 'dir').mockImplementation(_ => {})

beforeEach(() => jest.clearAllMocks())

describe('cli module', () => {
  it('exports cli function', () => {
    expect(typeof cli).toEqual('function')
    expect(cli.name).toEqual('cli')
  })

  describe('cli function', () => {
    it('returns 0 exitCode on success', async () => {
      expect(await cli([])).toEqual(0)
    })

    it('returns >0 exitCode on failure', async () => {
      expect(await cli(['-f'])).toEqual(1)
    })

    describe('handle errors gracefully', () => {
      it('outputs error messages to console.error', async () => {
        await expect(async _ => await cli(['-f'])).not.toThrow()
        expect(consoleDebugSpy).not.toHaveBeenCalled()
        expect(consoleErrorSpy).toHaveBeenCalledTimes(1)
        expect(consoleErrorSpy).toHaveBeenLastCalledWith(
          expect.stringContaining('Critical Error:')
        )
      })

      it('outputs errors to console.debug when debug is enabled', async () => {
        await expect(async _ => await cli(['-D', '-f'])).not.toThrow()
        expect(consoleErrorSpy).not.toHaveBeenCalled()
        expect(consoleDebugSpy).toHaveBeenCalledTimes(1)
        expect(consoleDebugSpy).toHaveBeenLastCalledWith(
          expect.stringContaining('DEBUG'),
          expect.any(Error)
        )
      })
    })
  })
})
