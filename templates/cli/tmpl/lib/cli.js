'use strict'

const { createConfigFromArgs } = require('./config')

async function cli (args) {
  process.exitCode = 0

  try {
    const config = await createConfigFromArgs(args)
    console.dir(config)
  } catch (error) {
    process.env.DEBUG && console.debug('DEBUG', error)
    process.env.DEBUG || console.error(`Critical Error: ${error.message}`)
    process.exitCode = 1
  }

  return process.exitCode
}

module.exports = exports = cli
