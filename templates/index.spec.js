'use strict'

const { isAbsolute } = require('path')

jest.unmock('./config.json')
const config = require('./config.json')
jest.unmock('./index.js')
const index = require('./index.js')

describe('Template index', () => {
  it('exports array of template directories', () => {
    expect(index).toHaveProperty(
      'templateDirs',
      expect.arrayContaining([expect.any(String)])
    )
  })

  it('exports parsed config.json', () => {
    expect(index).toHaveProperty('config')
    expect(index.config).toEqual(config)
  })

  it('exports absolute path to templates directory', () => {
    expect(index).toHaveProperty('path', __dirname)
    expect(isAbsolute(index.path)).toBe(true)
  })
})
