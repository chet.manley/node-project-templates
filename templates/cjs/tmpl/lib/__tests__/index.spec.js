'use strict'

jest.unmock('../index')
const index = require('../index')

describe('CJS template entrypoint', () => {
  it('is a sample test', () => {
    expect(typeof index).toEqual('function')
    expect(index.name).toEqual('cjsTemplate')
    expect(index()).toEqual('CJS template')
  })
})
