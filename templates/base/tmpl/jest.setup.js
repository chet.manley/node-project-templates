'use strict'

/**
 * ESlint error is raised by requiring a devDependency,
 * but this file is only used during testing.
 */
// eslint-disable-next-line node/no-unpublished-require
const stripAnsi = require('strip-ansi')

expect.addSnapshotSerializer({
  serialize: value => stripAnsi(value),
  test: value => Boolean(value) && typeof value === 'string'
})
