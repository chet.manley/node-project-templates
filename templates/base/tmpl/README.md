# {{ project.name }}

> **{{ project.description }}**

[![project vulnerabilities][vulnerabilities-badge]][vulnerabilities-url]
[![project dependencies][dependencies-badge]][dependencies-url]
[![code style standardjs][standardjs-badge]][standardjs-url]
[![versioning strategy][semver-badge]][semver-url]
[![required Node version][node-version-badge]][node-version-url]

[![CI pipeline status][ci-badge]][ci-url]
[![code coverage][coverage-badge]][coverage-url]

<!-- badge images and URLs -->
[ci-badge]: https://gitlab.com/{{ project.urlPath }}/badges/master/pipeline.svg
[ci-url]: https://gitlab.com/{{ project.urlPath }}
[coverage-badge]: https://gitlab.com/{{ project.urlPath }}/badges/master/coverage.svg?job=Code%20Coverage
[coverage-url]: https://{{ project.namespace }}.gitlab.io/{{ project.slug }}/master/coverage
[dependencies-badge]: https://img.shields.io/librariesio/release/npm/{{ project.name }}?logo=&style=for-the-badge
[dependencies-url]: https://www.npmjs.com/package/{{ project.name }}?activeTab=dependencies
[node-version-badge]: https://img.shields.io/node/v/{{ project.name }}?logo=&style=for-the-badge
[node-version-url]: https://nodejs.org/en/about/releases/
[semver-badge]: https://img.shields.io/static/v1?label=semver&message=standard-version&color=brightgreen&logo=&style=for-the-badge
[semver-url]: https://github.com/conventional-changelog/standard-version
[standardjs-badge]: https://img.shields.io/static/v1?label=style&message=standardJS&color=brightgreen&logo=&style=for-the-badge
[standardjs-url]: https://standardjs.com/
[vulnerabilities-badge]: https://img.shields.io/snyk/vulnerabilities/npm/{{ project.name }}?logo=&style=for-the-badge
[vulnerabilities-url]: https://snyk.io/vuln/search?q={{ project.name }}&type=npm

## Releases

[![latest release version][stable-release-badge]][npm-url]
[![next release version][next-release-badge]][npm-url]
[![package install size][install-size-badge]][install-size-url]

<!-- badge images and URLs -->
[install-size-badge]: https://flat.badgen.net/packagephobia/publish/{{ project.name }}
[install-size-url]: https://packagephobia.now.sh/result?p={{ project.name }}
[next-release-badge]: https://img.shields.io/npm/v/{{ project.name }}/next?label=@next&logo=npm&style=flat-square
[npm-url]: https://npmjs.org/package/{{ project.name }}
[stable-release-badge]: https://img.shields.io/npm/v/{{ project.name }}/latest?label=%40latest&logo=npm&style=flat-square

---

<!-- [[_TOC_]] -->

## Getting Started

### Installation

```shell
npm install {{ project.name }}
```

### Usage

```javascript
{{ how to use your package }}
```

## Built with

[![Fedora Linux](https://img.shields.io/static/v1?logo=fedora&label=Fedora&message=workstation&color=294172&style=flat)](https://getfedora.org/)
[![VSCode](https://img.shields.io/static/v1?logo=visual-studio-code&label=VSCode&message=insiders&color=2A917D&style=flat)](https://code.visualstudio.com/)
[![GitLab](https://img.shields.io/static/v1?logo=gitlab&label=GitLab&message=FOSS&color=DE4020&style=flat)](https://gitlab.com/gitlab-org/gitlab)
[![Caffeine](https://img.shields.io/static/v1?logo=buy-me-a-coffee&label=Caffeine&message=☕&color=603015&style=flat)](https://en.wikipedia.org/wiki/Caffeine)

## Contributing

The community is welcome to participate in this open source project.
Aspiring contributors should review the [contributing guide](https://gitlab.com/{{ project.urlPath }}/-/blob/master/CONTRIBUTING.md) for details on how to get started.
First-time contributors are encouraged to search for issues with the ~"good first issue" label.

## License

[![NPM][license-badge]][license-url]

Copyright © {{ year }} [{{ user.name }}](https://gitlab.com/{{ user.mention }}).

<!-- badge images and URLs -->
[license-badge]: https://img.shields.io/npm/l/{{ project.name }}
[license-url]: https://gitlab.com/{{ project.urlPath }}/-/blob/master/LICENSE

## Acknowledgements
