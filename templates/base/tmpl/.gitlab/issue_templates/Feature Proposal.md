# Problem to solve

<!--
> What problem are you trying to solve?
> Try to define the who/what/why of the problem, similar to a user story.
> For example, "As a (developer/end user), I want (feature/missing functionality), so I can (added value or problem resolution)."
-->

## Further details

<!--
> Include use cases, benefits, goals, or any relevant details that will help us understand the problem better.
-->

## Proposal

<!--
> Any further ideas you have that may help to resolve the issue.
> E.g., added properties, methods or configurations.
-->

### Applicable links and references

<!--
> Possible links to other projects that have solved this issue,
> or references describing similar ideas/solutions.
-->

/label ~feature
