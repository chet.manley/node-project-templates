#!/usr/bin/env sh

# Script must always be run from project root.
cd "${CI_PROJECT_DIR}"
lib=$(readlink -f '.gitlab/ci/scripts/lib/')

. "${lib}/api_get.sh"

# Beware of https://github.com/conventional-changelog/commitlint/issues/885
# if new project's first commit fails to lint.
#
# CI_PIPELINE_IID: "The unique id of the current pipeline scoped to project"
# HTTP status code: 404
sha=$(api_get "pipelines/${CI_PIPELINE_ID}" '.before_sha // empty')
ec=$?

# script is allowed to fail, so only print on success
[[ $ec -eq 0 ]] && printf '%s' "${sha}"

#[[ $ec -ne 0 ]] && exit $ec
exit 0
