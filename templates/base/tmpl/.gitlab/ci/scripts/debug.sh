#!/usr/bin/env sh

# Script must always be run from project root.
cd "${CI_PROJECT_DIR}"
lib=$(readlink -f '.gitlab/ci/scripts/lib/')

. "${lib}/cleanup.sh"
. "${lib}/get_package_version.sh"

trap 'cleanup' EXIT

version=$(get_package_version "${CI_PROJECT_DIR}/package.json")
echo "Exit Code: $?, version: @${CI_PROJECT_PATH}@${version}"
version=$(get_package_version)
echo "Exit Code: $?, version: @${CI_PROJECT_PATH}@${version}"

# script always fails, halting pipeline
exit 1
