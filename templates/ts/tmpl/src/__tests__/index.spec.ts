import index from '../index'
jest.unmock('../index')

describe('TypeScript template entrypoint', () => {
  it('is a sample test', () => {
    expect(typeof index).toEqual('function')
    expect(index.name).toEqual('tsTemplate')
    expect(index()).toEqual('TypeScript template')
  })
})
