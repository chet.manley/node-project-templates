import stripAnsi from 'strip-ansi'

expect.addSnapshotSerializer({
  serialize: value => stripAnsi(value),
  test: value => Boolean(value) && typeof value === 'string'
})
