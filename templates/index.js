'use strict'

const { lstatSync, readdirSync } = require('fs')
const { join } = require('path')

const config = require('./config.json')

const path = __dirname

function templatePredicate (absPath) {
  return lstatSync(absPath).isDirectory() && readdirSync(absPath).length !== 0
}

function templatesFilter (file) {
  return templatePredicate(join(path, file))
}

const templateDirs = readdirSync(path).filter(templatesFilter)

module.exports = exports = {
  config,
  path,
  templateDirs
}
