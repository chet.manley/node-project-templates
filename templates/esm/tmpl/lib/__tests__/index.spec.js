import index from '../index'
jest.unmock('../index')

describe('CJS template entrypoint', () => {
  it('is a sample test', () => {
    expect(typeof index).toEqual('function')
    expect(index.name).toEqual('cjsTemplate')
    expect(index()).toEqual('CJS template')
  })
})
