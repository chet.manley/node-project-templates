#This script is meant to be sourced!

# `lib` must be set in sourcing script
. "${lib}/get_http_status_code.sh"
. "${lib}/update_total_pages.sh"
. "${lib}/verify_api_response.sh"

api_get () {
  [[ -n "${1}" && -n "${2}" ]] || return 1
  local endpoint="${1}"
  local jq_filter="${2}"
  local query="${3}"

  local current_page=1
  local headers_file="/tmp/${CI_JOB_ID}-headers"
  local match=''
  local response=''
  local results_per_page=25
  local server_error=''
  local total_pages=1

  while [[ $((current_page <= total_pages)) -ne 0 ]]; do
    # Blocking: https://gitlab.com/gitlab-org/gitlab/issues/35067
    # -H "JOB-TOKEN: ${CI_JOB_TOKEN}"
    response=$(curl -sLD "${headers_file}" \
      -H "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
      -H "Accept: application/json" \
      -d "${query}" \
      -d "page=${current_page}" \
      -d "per_page=${results_per_page}" \
      -X 'GET' "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/${endpoint}" \
    )

    server_error=$(verify_api_response "${response}")
    # exit code 3: API response was invalid JSON
    if [[ $? -ne 0 ]]; then
      printf 'API returned invalid JSON. HTTP Status Code: %s' \
        $(get_http_status_code "${headers_file}")
      return 3
    fi
    # exit code 4: API returned an error message
    if [[ -n "${server_error}" ]]; then
      printf 'API error: %s' "${server_error}"
      return 4
    fi

    total_pages=$(update_total_pages "${headers_file}" "${total_pages}")

    match=$(printf '%s' "${response}" | jq -cjM "${jq_filter}")
    if [[ -n "${match}" ]]; then
      printf '%s' "${match}"
      break
    fi

    : $((current_page++))
  done

  # exit code 2: could not find a match
  [[ -z "${match}" ]] && return 2
  return 0
}
