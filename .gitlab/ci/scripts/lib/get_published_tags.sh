#This script is meant to be sourced!

get_published_tags () {
  local elapsed=0
  local filter="${1}"
  local start=$(date '+%s')
  local tags=''
  local timeout=10

  # wait for newly published packages to become available
  while [[ $elapsed -lt $timeout ]]; do
    tags=$(npm dist-tags ls "@${CI_PROJECT_PATH}" | grep "${filter}")
    [[ -n "${tags}" ]] && break
    elapsed=$(( $(date '+%s') - $start ))
    sleep 2
  done

  [[ -n "${tags}" ]] || return 1

  printf '%s' "${tags}"

  return 0
}
