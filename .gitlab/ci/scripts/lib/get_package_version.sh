#This script is meant to be sourced!

get_package_version () {
  local pkg_json="${1:-${CI_PROJECT_DIR}/package.json}"
  [[ -f "${pkg_json}" -a "$(basename ${pkg_json})" == 'package.json' ]] || return 1
  local version=$(jq -cjM '.version' "${pkg_json}")

  printf '%s' "${version}"

  return 0
}
