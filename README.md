# @chet.manley/node-project-templates

> **Templates used by the [@chet.manley/create-node-project][create-node-project] package.**

[![project vulnerabilities][vulnerabilities-badge]][vulnerabilities-url]
[![project dependencies][dependencies-badge]][dependencies-url]
[![code style standardjs][standardjs-badge]][standardjs-url]
[![versioning strategy][semver-badge]][semver-url]
[![required Node version][node-version-badge]][node-version-url]

[![CI pipeline status][ci-badge]][ci-url]
[![code coverage][coverage-badge]][coverage-url]

<!-- badge images and URLs -->
[ci-badge]: https://gitlab.com/chet.manley/node-project-templates/badges/master/pipeline.svg
[ci-url]: https://gitlab.com/chet.manley/node-project-templates
[coverage-badge]: https://gitlab.com/chet.manley/node-project-templates/badges/master/coverage.svg?job=Code%20Coverage
[coverage-url]: https://chet.manley.gitlab.io/node-project-templates/master/coverage
[dependencies-badge]: https://img.shields.io/librariesio/release/npm/@chet.manley/node-project-templates?logo=&style=for-the-badge
[dependencies-url]: https://www.npmjs.com/package/@chet.manley/node-project-templates?activeTab=dependencies
[node-version-badge]: https://img.shields.io/node/v/@chet.manley/node-project-templates?logo=&style=for-the-badge
[node-version-url]: https://nodejs.org/en/about/releases/
[semver-badge]: https://img.shields.io/static/v1?label=semver&message=standard-version&color=brightgreen&logo=&style=for-the-badge
[semver-url]: https://github.com/conventional-changelog/standard-version
[standardjs-badge]: https://img.shields.io/static/v1?label=style&message=standardJS&color=brightgreen&logo=&style=for-the-badge
[standardjs-url]: https://standardjs.com/
[vulnerabilities-badge]: https://img.shields.io/snyk/vulnerabilities/npm/@chet.manley/node-project-templates?logo=&style=for-the-badge
[vulnerabilities-url]: https://snyk.io/vuln/search?q=%40chet.manley%2Fnode-project-templates&type=npm

## Releases

[![latest release version][stable-release-badge]][npm-url]
[![next release version][next-release-badge]][npm-url]
[![package install size][install-size-badge]][install-size-url]

<!-- badge images and URLs -->
[install-size-badge]: https://flat.badgen.net/packagephobia/publish/@chet.manley/node-project-templates
[install-size-url]: https://packagephobia.now.sh/result?p=@chet.manley/node-project-templates
[next-release-badge]: https://img.shields.io/npm/v/@chet.manley/node-project-templates/next?label=@next&logo=npm&style=flat-square
[npm-url]: https://npmjs.org/package/@chet.manley/node-project-templates
[stable-release-badge]: https://img.shields.io/npm/v/@chet.manley/node-project-templates/latest?label=%40latest&logo=npm&style=flat-square

---

<!-- [[_TOC_]] -->
- [@chet.manley/node-project-templates](#chetmanleynode-project-templates)
  - [Releases](#releases)
  - [Quick Start](#quick-start)
    - [Install](#install)
  - [User Defined Templates](#user-defined-templates)
    - [Example Directory Structure](#example-directory-structure)
    - [Global Templates Configuration](#global-templates-configuration)
      - [Templates config.js[on]](#templates-configjson)
      - [Templates index.js](#templates-indexjs)
    - [Local Template Configuration](#local-template-configuration)
      - [Template config.js[on]](#template-configjson)
        - [Dependent Template Name](#dependent-template-name)
        - [Commit Message Template](#commit-message-template)
        - [Find and Replace](#find-and-replace)
        - [Template Files Directory](#template-files-directory)
        - [Package Manifest File](#package-manifest-file)
        - [Rename](#rename)
  - [Built with](#built-with)
  - [Contributing](#contributing)
  - [License](#license)

## Quick Start

### Install

> :warning:\
> **This package is a dependency of [@chet.manley/create-node-project][create-node-project].
> It is not meant to be a stand-alone project.
> However, this project can be cloned as a starting point for creating your own custom templates.**
>
> ```shell
> git clone https://gitlab.com/chet.manley/node-project-templates.git
> ```

## User Defined Templates

### Example Directory Structure

```text
📦 path/to/templates
 ┣📂 my-template
 ┃ ┣📂 tmpl
 ┃ ┃ ┣📂 bin
 ┃ ┃ ┃ ┗📜 cli
 ┃ ┃ ┣📜 .gitignore.template
 ┃ ┃ ┣📜 index.js
 ┃ ┃ ┣📜 LICENSE
 ┃ ┃ ┣📜 README.md
 ┃ ┃ ┗📜 pkg.json
 ┃ ┗📜 config.json
 ┣📂 base
 ┃ ┣📂 files
 ┃ ┃ ┗ ...
 ┃ ┗📜 config.json
 ┣📜 config.json
 ┗📜 index.js
```

> :warning:\
> **_In order to prevent multiple `package.json` files from being parsed by NPM during publishing
> (causing many files to be omitted in published package),
> template package manifest files must be saved under a different name._**

### Global Templates Configuration

#### Templates config.js[on]

> `optional: true`\
> `location: root templates directory`
>
> ```json
> {
>   "defaults": {
>     "manifestFile": "pkg.json",
>     "templateName": "base",
>     "templateFilesDir": "files"
>   }
> }
> ```
>
> | key               | type   | description                               |
> | :---------------- | :----- | :---------------------------------------- |
> | defaults          | object | Dictionary of default template options    |
> | .manifestFile     | string | File name of package manifest             |
> | .templateName     | string | Template to apply if none is selected     |
> | .templateFilesDir | string | Relative path to template files           |
>
> > :information_source:\
> > If this file is omitted, `index.js` must be present, and must return an appropriate config object.

#### Templates index.js

> `optional: true`\
> `location: root templates directory`
>
> A synchronous script that returns an object describing your template.
> Check [this project's index.js](templates/index.js) for an example of how this script should function.
> The returned object must hold the following shape:
>
> ```javascript
> {
>   config: {},
>   path: '',
>   templateDirs: []
> }
> ```
>
> | key          | type   | description                               |
> | :----------- | :----- | :---------------------------------------- |
> | config       | object | Output of config.js[on]                   |
> | path         | string | Absolute path to this templates directory |
> | templateDirs | array  | All valid template directories            |
>
> > :information_source:\
> > If this file is omitted, `config.js[on]` must be present,
> > and [@chet.manley/create-node-project][create-node-project] will attempt to manually enumerate templates.

### Local Template Configuration

#### Template config.js[on]

> `optional: true`\
> `location: root of each template directory`
>
> ```json
> {
>   "dependencyName": "base",
>   "commitTemplate": ".git-commit-template",
>   "far": [
>     "LICENSE",
>     "README.md"
>   ],
>   "filesDir": "tmpl",
>   "manifestFile": "pkg.json",
>   "name": "My Template",
>   "rename": {
>     ".gitignore.template": ".gitignore",
>     "bin/cli": "{{ project.slug }}"
>   }
> }
> ```
>
> | key             | type   | description                                     |
> | :-------------- | :----- | :---------------------------------------------- |
> | dependencyName  | string | Name of dependent template, or empty string     |
> | commitTemplate  | string | Path to file containing commit message template |
> | far             | array  | Files to perform find and replace in            |
> | filesDir        | string | Path to this template's files                   |
> | manifestFile    | string | File name of package manifest                   |
> | name            | string | Display name of template                        |
> | rename          | object | Dictionary of files to be renamed               |
>
> > :information_source:\
> > _If not required by your template, any of the keys may be omitted,
> > or the config file may be omitted entirely._

##### Dependent Template Name

> Defines a template that must be installed before this one,
> allowing granular template compostion via dependency chains (E.g., `cli ⇨ cjs ⇨ base`).
> With the exception of package manifests (which are merged),
> files in a template overwrite files in a dependency when their names match.
> While powerful, keep in mind that any missing dependency in the chain will cause the installation to fail.

##### Commit Message Template

> Path to a file containing a commit message template.
> The contents of this file will be displayed each time a commit is performed in your created projects. Example template can be found in
> [this project's commit template](.git-commit-message).

##### Find and Replace

> After template files have been copied to the target directory,
> a simple find and replace is performed. Inside your files,
> you may use placeholders formatted with double braces (E.g., `{{ variable }}`),
> that will be replaced at runtime with the value of the enclosed variable name.
> If the variable is undefined or cannot be found in the dictionary,
> the placeholder will be left unaltered.
>
> <!-- MD028 -->
>
> > ###### Find and Replace Dictionary
> >
> > | key path            | value                                             |
> > | :------------------ | :------------------------------------------------ |
> > | project.description | Copy of package.json `description` key            |
> > | project.name        | Name of your new project                          |
> > | project.namespace   | Repo namespace, username, or npm scope            |
> > | project.slug        | Package name minus namespace                      |
> > | project.urlPath     | Namespace+slug as it would appear in repo URL     |
> > | user.email          | Git user email                                    |
> > | user.mention        | Repo username as defined in Git credential config |
> > | user.name           | Git user's full name                              |
> > | year                | The current four digit year                       |

##### Template Files Directory

> The location of this template's files. Generally a single subdirectory,
> you can technically make it multiple subdirectories deep
> (E.g., `template/files/here`).
>
> :warning:\
> **This _must_ be a path relative to the config file.**

##### Package Manifest File

> A bug/quirk in the way NPM publishes packages causes all `package.json` files in a project to be parsed.
> This means a template's package manifest must use a different filename,
> whose contents will be merged into every new project's final `package.json` when templates are installed.

##### Rename

> Similar to find and replace, the key:value pairs define files to be renamed after your template's files have been copied to the target directory.
> The key must be a file path relative to the project root.
> The value must be either a key path found in the [find and replace dictionary](#find-and-replace-dictionary), or a filename.
> If using a filename, include _only_ the filename with no trailing path (all files are renamed in-place).
> Like find and replace, if a `{{ key.path }}` is provided and cannot be found in the dictionary, the file will remain as-is.

## Built with

[![Fedora Linux](https://img.shields.io/static/v1?logo=fedora&label=Fedora&message=workstation&color=294172&style=flat)](https://getfedora.org/)
[![VSCode](https://img.shields.io/static/v1?logo=visual-studio-code&label=VSCode&message=insiders&color=2A917D&style=flat)](https://code.visualstudio.com/)
[![GitLab](https://img.shields.io/static/v1?logo=gitlab&label=GitLab&message=FOSS&color=DE4020&style=flat)](https://gitlab.com/gitlab-org/gitlab)
[![Caffeine](https://img.shields.io/static/v1?logo=buy-me-a-coffee&label=Caffeine&message=☕&color=603015&style=flat)](https://en.wikipedia.org/wiki/Caffeine)

## Contributing

The community is welcome to participate in this open source project.
Aspiring contributors should review the [contributing guide](https://gitlab.com/chet.manley/node-project-templates/-/blob/master/CONTRIBUTING.md) for details on how to get started.
First-time contributors are encouraged to search for issues with the ~"good first issue" label.

## License

[![NPM][license-badge]][license-url]

Copyright © 2020 [Chet Manley](https://gitlab.com/chet.manley).

<!-- badge images and URLs -->
[license-badge]: https://img.shields.io/npm/l/@chet.manley/node-project-templates
[license-url]: https://gitlab.com/chet.manley/node-project-templates/-/blob/master/LICENSE

<!-- ## Acknowledgements -->

<!-- other URLs -->
[create-node-project]: https://gitlab.com/chet.manley/create-node-project/
